import express from "express";
import dotenv from "dotenv";
import loader from "./loaders";

dotenv.config();
const app = express();

async function startServ() {
    await loader(app);
}

startServ();
