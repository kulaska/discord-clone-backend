import jwt from "jsonwebtoken";
import config from "../config";
import { Request, Response } from "express";

export default function(req: Request, res: Response, next): void {
    const token = req.header("authorization");

    if (!token) {
        res.status(401).json({ msg: "No token, cannot authorize" });
    }

    try {
        const decodedJWT = jwt.verify(token.split(" ")[1], config.jwtSecret);
        req.userId = decodedJWT.userId;
        next();
    } catch (err) {
        res.status(401).json({ msg: "Invalid token, cannot authorize" });
    }
}
