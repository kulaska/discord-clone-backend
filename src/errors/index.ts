export class InputErr extends Error {
    constructor(message: string) {
        super(message);

        this.name = "InputErr";
    }
}

export class InvalidAuth extends Error {
    constructor(message: string) {
        super(message);

        this.name = "InvalidAuth";
    }
}

export class EmptyResponse extends Error {
    constructor(message: string) {
        super(message);

        this.name = "EmptyResponse";
    }
}
