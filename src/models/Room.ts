import mongoose from "mongoose";
import Message from "./Message";

const Room = new mongoose.Schema(
    {
        createdBy: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "User"
        },
        roomName: {
            type: String,
            required: [true, "Room name must be provided"]
        },
        messages: {
            type: [Message],
            default: []
        }
    },
    { timestamps: true }
);

export default mongoose.model("Room", Room);
