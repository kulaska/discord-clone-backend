import mongoose from 'mongoose';
import { IUser } from '../interfaces';

const User = new mongoose.Schema({
    username: {
        type: String,
        required: [true, "Username must be provided"],
        unique: true
    },
    
    password: {
        type: String,
        required: true
    },

    role: {
        type: String,
        default: 'user'
    },
},
    {timestamps: true}
);

export default mongoose.model<IUser>('User', User);