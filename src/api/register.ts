import express from "express";

import UserService from "../services/user";
import { validatePassword } from "./validators";

import { check, validationResult, body } from "express-validator";

const router = express.Router();

router.post(
    "/",
    [
        check("username", "username is required").notEmpty(),
        check("password", "password is required").isLength({ min: 6 }).custom(validatePassword)
    ],
    async (req: express.Request, res: express.Response) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(400).json({
                errors: errors.array()
            });
        }

        const { username, password } = req.body;

        const userServiceInstance = new UserService();

        try {
            const token = await userServiceInstance.registerNewUser({ username, password });
            res.json({ token });
        } catch (err) {
            if (err.name === "InputErr") {
                res.status(400).json({
                    errors: [
                        {
                            msg: "User already exists"
                        }
                    ]
                });
            } else {
                res.status(500).send("Internal server error");
            }
        }
    }
);

export default router;
