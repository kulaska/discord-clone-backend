export const validatePassword = (value: string) => {
  const stringWithoutNonNumeric = value
    .split("")
    .filter((char: String) => char >= "0" && char <= "9");
  return value.length >= 6 && stringWithoutNonNumeric.length < value.length - 1;
};
