import express from "express";
import ChatService from "../services/chat";
import checkAuth from "../middleware/checkAuth";

const router = express.Router();

router.post(
    "/register",
    checkAuth,
    async (req: express.Request, res: express.Response) => {
        const { roomName } = req.body;
        const { userId } = req;

        const chatServiceInstance = new ChatService();

        try {
            await chatServiceInstance.createNewRoom(roomName, userId);
            res.json({ msg: "The room has been successfully created" });
        } catch (err) {
            if (err.name === "InputErr")
                res.status(400).json({
                    errors: [{ msg: err.message }]
                });
            else res.status(500).end("Internal Server Error");
        }
    }
);

export default router;
