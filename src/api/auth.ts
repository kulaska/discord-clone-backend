import express from "express";

import AuthService from "../services/auth";

import { check, validationResult, body } from "express-validator";

const router = express.Router();

router.post(
    "/",
    [
        check("username", "username is empty").notEmpty(),
        check("password", "password is empty").notEmpty()
    ],
    async (req: express.Request, res: express.Response) => {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            res.status(400).json({
                errors: errors.array()
            });
        }

        const { username, password } = req.body;

        const authServiceInstance = new AuthService();

        try {
            const token = await authServiceInstance.authUser(
                username,
                password
            );
            res.json({ token });
        } catch (err) {
            if (err.name === "InputErr") {
                res.json({
                    errors: [{ msg: err.message }]
                });
            } else {
                res.status(500).send("Internal Server Error");
            }
        }
    }
);

export default router;
