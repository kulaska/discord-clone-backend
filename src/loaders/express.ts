import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import path from "path";
import io from "socket.io";
import http from "http";
import jwt from "jsonwebtoken";
import config from "../config";

import ChatService from "../services/chat";

import registerRoute from "../api/register";
import authRoute from "../api/auth";
import chatRoute from "../api/chat";

export default (expressApp: express.Application) => {
    /* Socket.io configuration */

    const httpServer = http.createServer(expressApp);
    const ioServer = io.listen(httpServer);
    httpServer.listen(3000);

    ioServer.on("connection", socket => {
        const chatServiceInstance = new ChatService();
        let userId = "";

        const updateUserId = (newUserId: string): string =>
            (userId = newUserId);

        socket.on("checkauth", message => {
            const decodedJWT = jwt.verify(message, config.jwtSecret);
            updateUserId(decodedJWT.id);
        });

        socket.on("roomCreate", roomId => {
            chatServiceInstance.createNewRoom(roomId, userId);
            socket.join(roomId);
        });

        socket.on("roomJoin", roomId => {
            socket.join(roomId);
        });

        socket.on("chat", async messageObject => {
            const { roomId, message: text, date } = messageObject;

            const message = {
                author: userId,
                roomId,
                text,
                date
            };

            ioServer.sockets.in(roomId).emit("new_message", message);

            await chatServiceInstance.pushMessageToDb(message);
        });
    });

    /* End of socket.io configuration */

    expressApp.use(bodyParser.json());
    expressApp.use("/api/users/register", registerRoute);
    expressApp.use("/api/users/auth", authRoute);
    expressApp.use("/api/chat", chatRoute);

    expressApp.use(
        morgan(
            ":method :url :status :res[content-length] - :response-time ms - :date[web]"
        )
    );

    expressApp.get("/", (req, res) => {
        res.sendFile("index.html", {
            root: path.join(__dirname, "../../public")
        });
    });

    expressApp.use(express.static("public"));

    expressApp.listen(process.env.PORT, function() {
        console.log("Listening");
    });
};
