import express from "express";
import mongooseLoader from "./mongoose";
import initExpress from './express';

export default async (expressApp: express.Application) => {
    initExpress(expressApp);
    await mongooseLoader();
};
