import mongoose from "mongoose";
import config from "../config";

export default async () => {
    try {
        await mongoose.connect(config.databaseURL);

        console.log("MongoDB successfully connected");
    } catch (err) {
        console.error(err);

        process.exit(1);
    }
};
