import dotenv from "dotenv";

process.env.NODE_ENV = process.env.NODE_ENV || "development";

const isEnv = dotenv.config();

if (!isEnv) {
    throw new Error("Couldn't find .env file");
}

export default {
    port: parseInt(process.env.PORT),

    databaseURL: process.env.DATABASE_URL || "",

    jwtSecret: process.env.JWT_SECRET || ""
};
