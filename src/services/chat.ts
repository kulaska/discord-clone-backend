import { IChatService, IMessage, IRoom } from "../interfaces";
import Room from "../models/Room";
import { InputErr, EmptyResponse } from "../errors";

export default class ChatService implements IChatService {
    public async createNewRoom(
        roomName: string,
        userId: string
    ): Promise<string> {
        const room = new Room({
            roomName,
            createdBy: userId
        });

        if (!roomName) {
            throw new InputErr("No room name was provided");
        }

        await room.save();

        return room.id;
    }

    public async getUserRooms(userId: string): Promise<IRoom[]> {
        const rooms = await Room.find({ createdBy: userId });

        if (!rooms) {
            throw new EmptyResponse("No results found for this id");
        }

        return rooms;
    }

    public async pushMessageToDb({
        roomId,
        text,
        date,
        author
    }: IMessage): Promise<void> {
        const room = await Room.findById(roomId);

        if (!room) {
            throw new InputErr("No room with such id exist");
        }

        room.messages.unshift({
            text,
            date,
            author
        });

        await room.save();
    }
}
