import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import User from "../models/User";
import { InputErr } from "../errors";
import { IPayload, IAuthService } from "../interfaces";
import config from "../config";

export default class AuthService implements IAuthService {
    public signJwt(payload: IPayload): string {
        return jwt.sign(payload, config.jwtSecret, { expiresIn: "1d" });
    }

    public async authUser(
        inputUsername: string,
        inputPassword: string
    ): Promise<string> {
        const user = await User.findOne({ username: inputUsername });

        if (!user) {
            throw new InputErr("User with this name doesn't exist");
        }

        const result = await bcrypt.compare(inputPassword, user.password);

        if (!result) {
            throw new InputErr("Password doesn't match");
        }

        const payload = { id: user.id };

        return this.signJwt(payload);
    }
}
