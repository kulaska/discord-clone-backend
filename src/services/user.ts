import bcrypt from "bcryptjs";
import { IUser, IAuthService, IUserService } from "../interfaces";
import { InputErr } from "../errors";
import User from "../models/User";
import AuthService from "./auth";

export default class UserService implements IUserService {
    private authService: IAuthService;

    constructor() {
        this.authService = new AuthService();
    }

    public async registerNewUser({ username, password }: IUser): Promise<string> {
        let user: IUser | null = await User.findOne({ username });

        if (user) {
            throw new InputErr("This username is already in use");
        }

        user = new User({
            username,
            password
        });

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password, salt);

        await user.save();

        const payload = {
            id: user.id
        };

        // Return new JWT token so the endpoint can send it to the client
        return this.authService.signJwt(payload);
    }
};
