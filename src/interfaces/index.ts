import mongoose from "mongoose";
export interface IUser extends mongoose.Document {
    username: string;
    password: string;
}

export interface IRoom extends mongoose.Document {
    createdBy: string;
    roomName: string;
}

export interface IPayload {
    id: string;
}

export interface IAuthService {
    signJwt(payload: IPayload): string;
    authUser(username: string, password: string): Promise<string>;
}

export interface IUserService {
    registerNewUser(user: IUser): Promise<string>;
}

export interface IMessage {
    author: string;
    text: string;
    date: Date;
    roomId: string;
}

export interface IChatService {
    createNewRoom(roomName: string, createdBy: string): Promise<string>;
    pushMessageToDb(message: IMessage): void;
}
